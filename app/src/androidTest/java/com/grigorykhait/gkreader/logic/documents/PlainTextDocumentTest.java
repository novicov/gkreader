/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.logic.documents;

import junit.framework.Assert;
import junit.framework.TestCase;

import java.io.IOException;

public class PlainTextDocumentTest extends TestCase {

    PlainTextDocument document;

    public PlainTextDocumentTest() throws IOException {
        String filename = "/sdcard/001.txt";
        document = new PlainTextDocument(filename);
    }

    public void testGetElements() throws Exception {

    }

    public void testGetElement() throws Exception {
       // Assert.assertEquals("<p>1</p>", document.getElement(0).getRepresentation());
       // Assert.assertEquals("<p>//</p>", document.getElement(4).getRepresentation());
    }

    public void testGetElementCount() throws Exception {
      //  Assert.assertEquals(5, document.getElementCount());
    }
}
