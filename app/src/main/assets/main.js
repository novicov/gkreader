var Document,
    currentPage,
    container,
    width,
    marginRight,
    numberOfPages,
    paper,
    text;

function startLoading() {
    container = document.getElementById("container");
        container.insertAdjacentHTML("beforeend", Document.getDocument());
    width = window.innerWidth;
    var style = container.currentStyle || window.getComputedStyle(document.getElementById("wrapper"));
    marginRight = parseInt(style.paddingRight.slice(0, -2));
    container.style.webkitColumnWidth = "" + width + "px";
    currentPage = 0;
    numberOfPages = (container.scrollWidth / width) >> 0;
    var header = document.getElementById("header");
    paper = Raphael(header, header.innerWidth, header.innerHeight);
    setPage(0);

}

function nextPage() {
    setPage(currentPage + 1);
}

function previousPage() {
    setPage(currentPage - 1);
}

function setPage(pageNum) {
    if (pageNum < 0 || pageNum >= numberOfPages) {
        return;
    }
    container.style.right = (width - marginRight) * pageNum + marginRight ;
    currentPage = pageNum;
    setPageText();
}

function setPageText() {
    if (!text) {
        text = paper.text(10, 10, "");
    }
    text.attr({
        "text": getCurrentPageNumber() + "/" + getNumberOfPages()
    });
}

function getNumberOfPages() {
    return numberOfPages;
}

function getCurrentPageNumber() {
    return currentPage + 1;
}