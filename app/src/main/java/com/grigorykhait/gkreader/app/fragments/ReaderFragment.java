/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.app.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.grigorykhait.gkreader.R;
import com.grigorykhait.gkreader.logic.documents.Document;

public class ReaderFragment extends BaseFragment implements View.OnTouchListener {

    private static final String LOG_TAG = ReaderFragment.class.getSimpleName();

    private static final String JAVASCRIPT_START_LOADING = "javascript:startLoading()";
    private static final String JAVASCRIPT_NEXT_PAGE = "javascript:nextPage()";
    private static final String JAVASCRIPT_PREVIOUS_PAGE = "javascript:previousPage()";
    Document mDocument;
    WebView mDocumentView;
    GestureDetector detector;

    public void setDocument(Document document) {
        Log.v(LOG_TAG, "setting document");
        mDocument = document;
        mDocumentView.loadUrl(JAVASCRIPT_START_LOADING);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        Log.v(LOG_TAG, "creating view");
        View rootView = inflater.inflate(R.layout.fragment_reader, container, false);
        mDocumentView = (WebView) rootView.findViewById(R.id.document_view);
        mDocumentView.getSettings().setJavaScriptEnabled(true);
        mDocumentView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                Log.v(LOG_TAG, "page started " + url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.v(LOG_TAG, "page finished " + url);
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
                Log.v(LOG_TAG, "loading resource " + url);
            }
        });
        mDocumentView.loadUrl("file:///android_asset/index.html");
        mDocumentView.addJavascriptInterface(new DocumentProxy(), "Document");
        mDocumentView.setOnTouchListener(this);

        detector = new GestureDetector(getActivity(), new CustomGestureDetector());
        return rootView;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return detector.onTouchEvent(event);
    }

    @Override
    public int getTitleResourceId() {
        return R.string.section_reader;
    }

    class DocumentProxy {

        @JavascriptInterface
        public String getDocument() {
            return mDocument.getRepresentation();
        }

    }

    class CustomGestureDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (e1 == null || e2 == null) return false;
            if (e1.getPointerCount() > 1 || e2.getPointerCount() > 1) return false;
            else {
                try { // right to left swipe .. go to next page
                    if (e1.getX() - e2.getX() > 100 && Math.abs(velocityX) > 800) {
                        mDocumentView.loadUrl(JAVASCRIPT_NEXT_PAGE);
                        return true;
                    } //left to right swipe .. go to prev page
                    else if (e2.getX() - e1.getX() > 100 && Math.abs(velocityX) > 800) {
                        mDocumentView.loadUrl(JAVASCRIPT_PREVIOUS_PAGE);
                        return true;
                    } //bottom to top, go to next document
                    else if (e1.getY() - e2.getY() > 100 && Math.abs(velocityY) > 800
                            && mDocumentView.getScrollY() >=
                            mDocumentView.getScale() * (
                                    mDocumentView.getContentHeight() - mDocumentView.getHeight())) {
                        //do your stuff
                        return true;
                    } //top to bottom, go to prev document
                    else if (e2.getY() - e1.getY() > 100 && Math.abs(velocityY) > 800) {
                        //do your stuff
                        return true;
                    }
                } catch (Exception e) { // nothing
                }
                return false;
            }
        }
    }

}
