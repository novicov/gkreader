/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.app.data;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by Григорий on 19.12.2014.
 */
public class DocumentContract {

    public static final String CONTENT_AUTHORITY = "com.grigorykhait.gkreader";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    private static final String PATH_DOCUMENTS = "documents";
    private static final String PATH_GENRES = "genres";
    private static final String PATH_SERIES = "series";
    private static final String PATH_AUTHORS = "authors";

    public static class Document implements BaseColumns,
            DocumentColumns {

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/vnd.gkreader.document";
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/vnd.gkreader.document";

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_DOCUMENTS).build();

        public static Uri buildDocumentUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static long getDocumentId(Uri uri) {
            return Long.parseLong(uri.getPathSegments().get(1));
        }

    }

    public static class Author implements BaseColumns, AuthorColumns {

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/vnd.gkreader.author";
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/vnd.gkreader.author";

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_AUTHORS).build();

        public static Uri buildAuthorUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri buildAuthorWithName(String name) {
            return CONTENT_URI.buildUpon().appendEncodedPath(name).build();
        }

        public static long getAuthorId(Uri uri) {
            return Long.parseLong(uri.getPathSegments().get(1));
        }
    }

    public static class Genre implements BaseColumns, GenreColumns {

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/vnd.gkreader.genre";
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/vnd.gkreader.genre";

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_GENRES).build();

        public static Uri buildGenreUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static long getGenreId(Uri uri) {
            return Long.parseLong(uri.getPathSegments().get(1));
        }
    }

    public static class Series implements BaseColumns, SeriesColumns {

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/vnd.gkreader.series";
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/vnd.gkreader.series";

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_SERIES).build();

        public static Uri buildSeriesUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static long getSeriesId(Uri uri) {
            return Long.parseLong(uri.getPathSegments().get(1));
        }
    }

    public static interface AuthorColumns {
        String NAME = "name";
    }

    public static interface DocumentColumns {
        String TITLE = "title";
        String DESCRIPTION = "description";
        String MIME_TYPE = "mime_type";
        String FILE_URI = "file_uri";
        String COVER = "cover";
        String LANG = "lang";
        String LAST_VIEWED = "last_viewed";
        String LAST_MODIFIED = "last_modified";
    }

    public static interface GenreColumns {
        String GENRE = "name";
    }

    public static interface SeriesColumns {
        String TITLE = "title";
    }

    public static interface DocumentsFullQuery extends DocumentColumns{
        String _ID = "documents._id";
        String TITLE = "documents.title";
        String AUTHORS = "authors";
        String GENRES = "genres";
        String SERIES = "series";

    }
}
