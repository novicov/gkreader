/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.app;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import com.grigorykhait.gkreader.logic.documents.Document;
import com.grigorykhait.gkreader.logic.documents.FictionBookDocument;
import com.grigorykhait.gkreader.logic.documents.PlainTextDocument;

import java.io.IOException;

public class DocumentLoader extends AsyncTaskLoader<Document> {

    private final static String LOG_TAG = DocumentLoader.class.getSimpleName();
    private String mFilePath;

    /**
     * Stores away the application context associated with context.
     * Since Loaders can be used across multiple activities it's dangerous to
     * store the context directly; always use {@link #getContext()} to retrieve
     * the Loader's Context, don't use the constructor argument directly.
     * The Context returned by {@link #getContext} is safe to use across
     * Activity instances.
     *
     * @param context  used to retrieve the application context.
     * @param filePath
     */
    public DocumentLoader(Context context, String filePath) {
        super(context);
        mFilePath = filePath;
    }


    @Override
    public Document loadInBackground() {
        try {
            if (mFilePath.endsWith(".txt")) {
                Log.v(LOG_TAG, "txt");
                return new PlainTextDocument(mFilePath);
            } else if (mFilePath.endsWith(".fb2") || mFilePath.endsWith(".fb2.zip")) {
                Log.v(LOG_TAG, "fb2");
                return new FictionBookDocument(mFilePath);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
