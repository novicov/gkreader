/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.app.library;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.grigorykhait.gkreader.app.data.DocumentContract;
import com.grigorykhait.gkreader.app.utils.Utils;
import com.grigorykhait.gkreader.logic.documents.Document;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class LibraryReader {

    private static final String LOG_TAG = LibraryReader.class.getSimpleName();

    Context mContext;
    String mLibraryPath;

    public LibraryReader(Context context, String libraryPath) {
        mContext = context;
        mLibraryPath = libraryPath;
    }

    public List<File> createLibrary() {
        File libraryDirectory = new File(mLibraryPath);
        if (!libraryDirectory.exists()) {
            throw new IllegalArgumentException("Folder does not exist");
        }
        if (!libraryDirectory.isDirectory()) {
            throw new IllegalArgumentException("Path doesn't represent folder");
        }
        return processFile(libraryDirectory);
    }

    private List<File> processFile(File fileToProcess) {
        List<File> resFiles = new ArrayList<>();
        if (fileToProcess.isDirectory()) {
            for (File f : fileToProcess.listFiles()) {
                resFiles.addAll(processFile(f));
            }
            return resFiles;
        }

        if (Utils.isAllowed(fileToProcess)) {
            resFiles.add(fileToProcess);
        }
        return resFiles;
        //String mimeType = Utils.getMimeType(mContext, fileToProcess.getPath());
        /*String filePath = fileToProcess.getPath();
        Document document = null;
        try {
            if (filePath.endsWith(".fb2.zip") || filePath.endsWith(".fb2")) {
                document = new FictionBookDocument(filePath, true);
            } else if (filePath.endsWith(".fb3")) {

            } else if (filePath.endsWith(".txt")) {
                document = new PlainTextDocument(filePath);
            } else if (filePath.endsWith(".epub")) {

            }
            if (document != null) {
                Log.v(LOG_TAG, "Found file " + filePath);
                storeDocument(document);
            }
        } catch (IOException exc) {

        }*/
    }

    private void storeDocument(Document document) {
        ContentResolver resolver = mContext.getContentResolver();
        Cursor c = resolver.query(DocumentContract.Document.CONTENT_URI,
                null,
                DocumentContract.DocumentColumns.FILE_URI + "=?",
                new String[]{document.getFile().getPath()},
                null);
        if (c.getCount() == 0) {
            ContentValues values = new ContentValues();
            values.put(DocumentContract.Document.FILE_URI, document.getFile().getPath());
            values.put(DocumentContract.Document.DESCRIPTION, document.getAnnotation());
            values.put(DocumentContract.Document.TITLE, document.getTitle());
            values.put(DocumentContract.Document.LANG, document.getLanguageCode());
            values.put(DocumentContract.Document.LAST_MODIFIED, document.getFile().lastModified());
            Uri uri = mContext.getContentResolver()
                    .insert(DocumentContract.Document.CONTENT_URI, values);
            Log.v(LOG_TAG, "File " + document.getFile() + " inserted " + uri);
        } else if (c.getCount() == 1) {

        }
    }
}
