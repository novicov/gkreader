/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.app.fragments;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.grigorykhait.gkreader.R;
import com.grigorykhait.gkreader.app.OnFragmentInteractionListener;
import com.grigorykhait.gkreader.app.data.DocumentContract;
import com.grigorykhait.gkreader.app.data.model.DocumentItem;
import com.grigorykhait.gkreader.app.library.LibraryAdapter;
import com.grigorykhait.gkreader.app.services.LibraryService;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.grigorykhait.gkreader.app.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class LibraryFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String LOG_TAG = LibraryFragment.class.getSimpleName();

    private OnFragmentInteractionListener mListener;

    private RecyclerView mRecyclerView;
    private LibraryAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public LibraryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String directory = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Books/";
        Log.v(LOG_TAG, directory);
        LibraryService.startActionFoo(getActivity(), directory);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_library, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.library_view);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new LibraryAdapter(getActivity());

        if (mRecyclerView != null) {
            mRecyclerView.setAdapter(mAdapter);
        }
        Log.v(LOG_TAG, "Creating loader");
        LoaderManager loaderManager = getLoaderManager();
        Loader loader = loaderManager.initLoader(0, null, this);
        loader.forceLoad();
        return rootView;
    }

    @Override
    public int getTitleResourceId() {
        return R.string.section_library;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.v(LOG_TAG, "onCreateLoader");
        return new CursorLoader(getActivity(),
                DocumentContract.Document.CONTENT_URI.buildUpon().appendPath("full").build(),
                null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // ArrayList<Card> cards= new ArrayList<>()
        Log.v(LOG_TAG, "Finished loading");
        while (data.moveToNext()){
            mAdapter.add(DocumentItem.createFromCursor(data));
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Log.v(LOG_TAG, "onLoaderReset");
        mAdapter.clear();
    }
}
