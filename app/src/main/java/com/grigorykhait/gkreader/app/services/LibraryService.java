/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.app.services;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

import com.grigorykhait.gkreader.app.data.DocumentContract;
import com.grigorykhait.gkreader.app.data.model.DocumentItem;
import com.grigorykhait.gkreader.app.library.LibraryReader;
import com.grigorykhait.gkreader.logic.documents.Document;
import com.grigorykhait.gkreader.logic.documents.FictionBookDocument;
import com.grigorykhait.gkreader.logic.documents.PlainTextDocument;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * helper methods.
 */
public class LibraryService extends IntentService {

    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_FOO = "com.grigorykhait.gkreader.app.services.action.FOO";

    // TODO: Rename parameters
    private static final String LIBRARY_FOLDER = "com.grigorykhait.gkreader.app.services.extra.PARAM1";
    private static final String LOG_TAG = LibraryService.class.getSimpleName();

    public LibraryService() {
        super("LibraryService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionFoo(Context context, String libraryFolder) {
        Intent intent = new Intent(context, LibraryService.class);
        intent.setAction(ACTION_FOO);
        intent.putExtra(LIBRARY_FOLDER, libraryFolder);
        context.startService(intent);
    }

    private static DocumentItem getByUri(List<DocumentItem> documents, String uri) {
        for (int i = 0, documentsSize = documents.size(); i < documentsSize; i++) {
            DocumentItem document = documents.get(i);
            if (document.fileUri.equals(uri)) {
                documents.remove(i);
                return document;
            }
        }
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String libraryDirectory = intent.getStringExtra(LIBRARY_FOLDER);
            readLibrary(libraryDirectory);

        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void readLibrary(String libraryDirectory) {
        LibraryReader libraryReader = new LibraryReader(getApplicationContext(), libraryDirectory);
        List<File> library = libraryReader.createLibrary();
        Cursor data = getContentResolver().query(DocumentContract.Document.CONTENT_URI,
                null,
                null,
                null,
                null);
        List<DocumentItem> documents = getDocumentsFromCursor(data);
        for (File file : library) {
            DocumentItem item = getByUri(documents, file.getPath());
            //Log.v(LOG_TAG, file.getPath());
            if (item == null || item.lastModified.getTime() < file.lastModified()) {
                try {
                    Document document;
                    String filePath = file.getPath();
                    if (filePath.endsWith(".txt")) {
                        document = new PlainTextDocument(filePath);
                    } else if (filePath.endsWith(".fb2") || filePath.endsWith(".fb2.zip")) {
                        document = new FictionBookDocument(filePath, true);
                    } else {
                        document = new PlainTextDocument(filePath);
                    }
                    if (item != null) {
                        item = DocumentItem.createFromDocument(document, item.id);
                    } else {
                        item = DocumentItem.createFromDocument(document);
                    }
                    item.save(getApplication());
                } catch (IOException e) {
                }
            }
        }
        for (DocumentItem item : documents){
            item.remove(getApplication());
        }

    }

    private List<DocumentItem> getDocumentsFromCursor(Cursor cursor) {
        List<DocumentItem> documents = new ArrayList<>();
        while (cursor.moveToNext()) {
            documents.add(DocumentItem.createFromCursor(cursor));
        }
        return documents;
    }
}
