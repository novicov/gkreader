package com.grigorykhait.gkreader.app;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.grigorykhait.gkreader.R;
import com.grigorykhait.gkreader.app.fragments.ReaderFragment;
import com.grigorykhait.gkreader.logic.documents.Document;

import java.io.File;

public class ReaderActivity extends Activity implements LoaderManager.LoaderCallbacks<Document> {

    private static final String LOG_TAG = ReaderActivity.class.getSimpleName();

    private static final String OPENED_FILE_URI_KEY = "file_uri_key";

    private Document mDocument;
    private ReaderFragment mReaderFragment = new ReaderFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.v(LOG_TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reader);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, mReaderFragment)
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reader, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean checkDocumentIsOpened(String filePath) {
        if (mDocument == null) return false;
        File newFile = new File(filePath);
        return newFile.lastModified() == mDocument.getFile().lastModified();
    }

    @Override
    public Loader<Document> onCreateLoader(int id, Bundle args) {
        Log.v(LOG_TAG, "Creating a loader");
        if (args == null) throw new IllegalArgumentException();
        String filePath = args.getString(OPENED_FILE_URI_KEY);
        if (checkDocumentIsOpened(filePath)) {
            Log.v(LOG_TAG, "Opening the same file");
            return null;
        }
        return new DocumentLoader(this, filePath);
    }

    @Override
    public void onLoadFinished(Loader<Document> loader, Document document) {
        Log.v(LOG_TAG, "Loading finished");
        mDocument = document;
        mReaderFragment.setDocument(document);
    }


    @Override
    public void onLoaderReset(Loader<Document> loader) {

    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.v(LOG_TAG, "onNewIntent");
        super.onNewIntent(intent);
        this.setIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        processIntent(getIntent());
    }

    private void processIntent(Intent intent) {
        if (intent != null) {
            Uri fileUri = intent.getData();
            if (fileUri != null) {
                Log.v(LOG_TAG, fileUri.toString());
                LoaderManager loaderManager = getLoaderManager();
                Bundle loaderParameter = new Bundle();
                loaderParameter.putString(OPENED_FILE_URI_KEY, fileUri.getPath());
                if (loaderManager.getLoader(0) == null) {
                    loaderManager.initLoader(0, loaderParameter, this).forceLoad();
                } else {
                    loaderManager.restartLoader(0, loaderParameter, this).forceLoad();
                }
            }
        }
    }
}
