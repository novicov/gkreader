/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.logic.documents;

import android.graphics.Bitmap;

import com.grigorykhait.gkreader.logic.parsers.PlainTextParser;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

/**
 * Created by Григорий on 28.12.2014.
 */
public class PlainTextDocument extends Document {

    private PlainTextParser mParser;
    private String mFilePath;

    public PlainTextDocument(String filePath) throws IOException {
        super(filePath);
        try {
            mParser = new PlainTextParser(new FileReader(filePath));
            setRootNode(mParser.parse());
            mFilePath = filePath;
        } catch (FileNotFoundException exc) {
            throw new IllegalArgumentException("File path" + filePath + " is not valid");
        }
    }

    @Override
    public String getTitle() {
        return mFilePath;
    }

    @Override
    public Bitmap getCover() {
        return null;
    }

    @Override
    public String getAnnotation() {
        return null;
    }

    @Override
    public List<String> getAuthors() {
        return null;
    }

    @Override
    public List<String> getGenres() {
        return null;
    }

    @Override
    public String getLanguageCode() {
        return null;
    }

    @Override
    public String getRepresentation() {
        if (getRootNode()!=null){
            return getRootNode().getRepresentation();
        }
        return "";
    }


}
