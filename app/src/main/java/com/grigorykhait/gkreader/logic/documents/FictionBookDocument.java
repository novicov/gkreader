/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.logic.documents;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.grigorykhait.gkreader.logic.nodes.fb2.AnnotationNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.BinaryNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.FullRootNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.RootNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.content.ImageNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.description.DescriptionNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.description.TitleInfoNode;
import com.grigorykhait.gkreader.logic.parsers.FictionBookParser;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by Григорий on 05.03.2015.
 */
public class FictionBookDocument extends Document {

    private static final String LOG_TAG = FictionBookDocument.class.getSimpleName();

    FictionBookParser mParser;

    public FictionBookDocument(String filePath) throws IOException {
        this(filePath, false);
    }

    public FictionBookDocument(String filePath, boolean descriptionOnly) throws IOException {
        super(filePath);
        InputStream in;
        String extension = filePath.substring(filePath.lastIndexOf(".") + 1);
        if (extension.equals("zip")) {
            ZipFile zipFile = new ZipFile(filePath);
            if (zipFile.size() == 0 || zipFile.size() > 1) {
                throw new IllegalArgumentException("");
            }
            ZipEntry zipEntry = zipFile.entries().nextElement();
            in = zipFile.getInputStream(zipEntry);
        } else {
            in = new FileInputStream(filePath);
        }
        Log.v(LOG_TAG, "Before parsing");
        mParser = new FictionBookParser(in, descriptionOnly);
        setRootNode(mParser.parse());
    }

    @Override
    public RootNode getRootNode() {
        return (RootNode) super.getRootNode();
    }

    @Override
    public List<String> getGenres() {
        return getDescription().getTitleInfo().getGenres();
    }

    @Override
    public String getLanguageCode() {
        return getTitleInfo().getLang();
    }

    @Override
    public String getRepresentation() {
        return getRootNode().getRepresentation();
    }

    @Override
    public String getTitle() {
        return getDescription().getTitleInfo().getBookTitle();
    }

    @Override
    public Bitmap getCover() {
        List<ImageNode> coverNode = getTitleInfo().getCovers();
        if (coverNode == null) {
            return null;
        }
        BinaryNode image = getRootNode().findImageById(coverNode.get(0).getHref());
        if (image == null) {
            return null;
        }
        byte[] imageData = image.getData();
        return BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
    }

    @Override
    public String getAnnotation() {
        AnnotationNode a = getTitleInfo().getAnnotation();
        if (a!=null){
            return a.getRepresentation();
        }
        return "";
    }

    @Override
    public List<String> getAuthors() {
        return getDescription().getTitleInfo().getAuthors();
    }

    private DescriptionNode getDescription() {
        return getRootNode().getDescription();
    }

    private TitleInfoNode getTitleInfo() {
        return getDescription().getTitleInfo();
    }

}
