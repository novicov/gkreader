/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.logic.nodes.fb2.description;

import com.grigorykhait.gkreader.logic.nodes.fb2.AnnotationNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.DateNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.Fb2Node;
import com.grigorykhait.gkreader.logic.nodes.fb2.content.ImageNode;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by grffn on 19.03.2015.
 */
public class TitleInfoNode extends Fb2Node {

    @ElementList(entry = "genre", inline = true, required = false)
    private List<String> genres;

    @ElementList(entry = "author", inline = true)
    private List<AuthorNode> authors;

    @Element(name = "book-title")
    private String bookTitle;

    @Element(name = "annotation", required = false)
    private AnnotationNode annotation;

    @ElementList(name = "keywords", required = false)
    private List<String> keywords;

    @Element(name = "date", required = false)
    private DateNode date;

    @Path("coverpage")
    @ElementList(required = false, inline = true, entry = "image")
    private List<ImageNode> covers;

    @Element(name = "lang")
    private String lang;

    @Element(name = "src-lang", required = false)
    private String srcLang;

    @ElementList(entry = "translator", required = false, inline = true)
    private List<AuthorNode> translators;

    @ElementList(entry = "sequence", required = false, inline = true)
    private List<SequenceNode> sequences;

    public List<String> getGenres() {
        return genres;
    }

    public List<String> getAuthors() {
        List<String> authorsList = new ArrayList<>(1);
        for (AuthorNode a : authors) {
            authorsList.add(a.getAuthorName());
        }
        return authorsList;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public AnnotationNode getAnnotation() {
        return annotation;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public DateNode getDate() {
        return date;
    }

    public List<ImageNode> getCovers() {
        return covers;
    }

    public String getLang() {
        return lang;
    }

    public String getSrcLang() {
        return srcLang;
    }

    public List<AuthorNode> getTranslators() {
        return translators;
    }

    public List<SequenceNode> getSequences() {
        return sequences;
    }

    @Override
    public String getRepresentation() {
        return null;
    }

}
