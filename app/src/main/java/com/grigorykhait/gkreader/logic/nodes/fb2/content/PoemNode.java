/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.logic.nodes.fb2.content;

import com.grigorykhait.gkreader.logic.nodes.fb2.DateNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.EpigraphNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.Fb2Node;
import com.grigorykhait.gkreader.logic.nodes.fb2.StanzaNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.TitleNode;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.ElementListUnion;

import java.util.List;

/**
 * Created by grffn on 18.03.2015.
 */
public class PoemNode extends ContentNode {


    @Attribute(required = false, name = "id")
    private String id;

    @Element(required = false)
    private TitleNode title;

    @ElementList(required = false, inline = true)
    private List<EpigraphNode> epigraphList;

    @ElementListUnion({
            @ElementList(entry = "subtitle", inline = true, type = PNode.class, required = false),
            @ElementList(entry = "stanza", inline = true, type = StanzaNode.class, required = false),

    })
    private List<Fb2Node> elementList;

    @ElementList(entry = "text-author", inline = true, required = false)
    private List<PNode> textAuthor;

    @ElementList(entry = "date", required = false)
    private DateNode date;


    @Override
    public String getRepresentation() {
        return "not implemented";
    }

    public String getId() {
        return id;
    }
}
